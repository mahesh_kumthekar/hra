<?php
use App\Models\SystemModel;
use App\Models\CompanysettingsModel;
$SystemModel = new SystemModel();
$CompanysettingsModel = new CompanysettingsModel();
$xin_system = $SystemModel->where('setting_id', 1)->first();
$xin_com_system = $CompanysettingsModel->where('setting_id', 1)->first();

$favicon = base_url().'/public/uploads/logo/favicon/'.$xin_system['favicon'];
$session = \Config\Services::session();
$request = \Config\Services::request();
$username = $session->get('sup_username');

?>
<?= doctype(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>
<?= $title; ?>
</title>
<!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 11]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="" />
<meta name="keywords" content="">
<meta name="author" content="erp" />

<!-- Favicon icon -->
<link rel="icon" type="image/x-icon" href="<?= $favicon;?>">

<!-- font css -->
<link rel="stylesheet" href="<?= base_url('public/assets/fonts/font-awsome-pro/css/pro.min.css');?>">
<link rel="stylesheet" href="<?= base_url('public/assets/fonts/feather.css');?>">
<link rel="stylesheet" href="<?= base_url('public/assets/fonts/fontawesome.css');?>">

<!-- vendor css -->
<link rel="stylesheet" href="<?= base_url('public/assets/css/style.css');?>">
<link rel="stylesheet" href="<?= base_url('public/assets/css/customizer.css');?>">
<link rel="stylesheet" href="<?= base_url('public/assets/plugins/toastr/toastr.css');?>">
</head>

<body>

<?php

  echo view('erp/auth/register_page');

?>
<!-- [ auth-sign ] end --> 

<!-- Required Js --> 
<script src="<?= base_url('public/assets/js/vendor-all.min.js');?>"></script> 
<script src="<?= base_url('public/assets/js/plugins/bootstrap.min.js');?>"></script> 
<script src="<?= base_url('public/assets/js/plugins/feather.min.js');?>"></script> 
<script src="<?= base_url('public/assets/js/pcoded.min.js');?>"></script> 
<script src="<?= base_url();?>/public/assets/plugins/toastr/toastr.js"></script> 
<script src="<?= base_url();?>/public/assets/plugins/sweetalert2/sweetalert2@10.js"></script>
<link rel="stylesheet" href="<?= base_url();?>/public/assets/plugins/ladda/ladda.css">
<script src="<?= base_url();?>/public/assets/plugins/spin/spin.js"></script> 
<script src="<?= base_url();?>/public/assets/plugins/ladda/ladda.js"></script> 
<script type="text/javascript">
  var desk_url = '<?php echo site_url('erp/login'); ?>';
var processing_request = '<?= lang('Login.login_to_acc');?>';
  $(document).ready(function() {

    /* Add data */ /*Form Submit*/
  $("#xin-form1").submit(function(e){
    var fd = new FormData(this);
    var obj = $(this), action = obj.attr('name');
    fd.append("is_ajax", 1);
    fd.append("type", 'add_record');
    fd.append("form", action);
    e.preventDefault(); 
  
    $.ajax({
      url: e.target.action,
      type: "POST",
      data:  fd,
      contentType: false,
      cache: false,
      processData:false,
      success: function(JSON)
      {
        if (JSON.error != '') {
          toastr.error(JSON.error);
          $('input[name="csrf_token"]').val(JSON.csrf_hash);
          Ladda.stopAll();
        } else {
         
            toastr.success(JSON.result);
         Ladda.stopAll();
        var timerInterval;
        Swal.fire({
          title:JSON.result,
          html:processing_request,
          timer:2000,
          icon: "success",
          showConfirmButton: false,
          timerProgressBar: true,
          onBeforeOpen:function(){
            Swal.showLoading(),
            t=setInterval(function(){
              },100)}
            ,onClose:function(){clearInterval(t);
            window.location = desk_url;
            }})
            .then(function(t){
              t.dismiss===Swal.DismissReason.timer&&console.log("I was closed by the timer")});
        e.preventDefault();
        }
      },
      error: function() 
      {
        toastr.error(JSON.error);
        $('input[name="csrf_token"]').val(JSON.csrf_hash);
          Ladda.stopAll();
      }           
     });
  });
});
</script>

</body></html>