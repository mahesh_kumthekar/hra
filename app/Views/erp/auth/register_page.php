<?php
use App\Models\SystemModel;
$SystemModel = new SystemModel();
$xin_system = $SystemModel->where('setting_id', 1)->first();
$favicon = base_url().'/public/uploads/logo/favicon/'.$xin_system['favicon'];
$session = \Config\Services::session();
$request = \Config\Services::request();
$username = $session->get('sup_username');

?>
<!-- [ auth-signin ] start -->
<div class="auth-wrapper auth-v3">
  <div class="auth-content">
    <?php if($session->get('err_not_logged_in')){?>
    <div class="alert alert-danger alert-dismissible fade show">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <?= $session->get('err_not_logged_in');?>
    </div>
    <?php } ?>
    <?php if($request->getVar('v')){?>
    <div class="alert alert-success alert-dismissible fade show">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <?= lang('Frontend.xin_your_account_is_verified');?>
    </div>
    <?php } ?>
    <div class="card">
      <div class="row align-items-stretch text-center">
        <div class="col-md-6 img-card-side"> <img src="<?= base_url('public/assets/images/auth/'.$xin_system['auth_background'].'.jpg');?>" alt="" class="img-fluid"> </div>
        <div class="col-md-6">
          <div class="card-body">
            <div class="text-left">
              <h4 class="mb-3 f-w-600">
                <?= lang('Login.xin_welcome_to');?>
                <span class="text-primary">
                <?= $xin_system['company_name'];?>
                </span></h4>
              <p class="text-muted">
                
              </p>
            </div>
            <?php $attributes = array('name' => 'add_employee', 'id' => 'xin-form1', 'autocomplete' => 'off');?>
    <?php $hidden = array('user_id' => 0,'type'=>'add_record','form'=>'add_employee','is_ajax'=>1);?>
    <?= form_open_multipart('erp/employees/sign_up', $attributes, $hidden);?>
            <div class="">
              <div class="input-group mb-3">
               <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-user"></i></span></div>
                 <input type="text" class="form-control" placeholder="<?= lang('Main.xin_employee_first_name');?>" name="first_name">
              </div>
              <div class="input-group mb-4">
                <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-user"></i></span></div>
                <input type="text" class="form-control" placeholder="<?= lang('Main.xin_employee_last_name');?>" name="last_name">
              </div>
              <div class="input-group mb-3">
                 <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-phone-alt"></i></span></div>
                 <input class="form-control" placeholder="<?= lang('Main.xin_contact_number');?>" name="contact_number" type="text">
              </div>
              <div class="input-group mb-4">
                <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-user"></i></span></div>
                <select class="form-control" name="gender" data-plugin="select_hrm" data-placeholder="<?= lang('Main.xin_employee_gender');?>">
                  <option value="">Select
                  </option>
                    <option value="1">
                    <?= lang('Main.xin_gender_male');?>
                    </option>
                    <option value="2">
                    <?= lang('Main.xin_gender_female');?>
                    </option>
                  </select>

              </div>
              <div class="input-group mb-3">
                 <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-envelope"></i></span></div>
                    <input class="form-control" placeholder="<?= lang('Main.xin_email');?>" name="email" type="text">
              </div>
              <div class="input-group mb-4">
                 <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-user"></i></span></div>
                    <input class="form-control" placeholder="<?= lang('Main.dashboard_username');?>" name="username" type="text">
              </div>
               <div class="input-group mb-4">
                <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-eye-slash"></i></span></div>
                    <input class="form-control" placeholder="<?= lang('Main.xin_employee_password');?>" name="password" type="password">
              </div>
              
            </div>
            <div class="text-right mt-2">
              <button type="submit" class="btn btn-primary mt-2"><i class="fas fa-user-lock d-blockd"></i>
              <?= lang('Login.xin_register');?>
              </button>
            </div>
            <div class="form-group text-left my-2">
                <div class="float-right"> <a href="<?= site_url('erp/login');?>" class="text-primary"><span>
                  <?= lang('Login.xin_login');?>
                  </span></a> </div>
                <div class="d-inline-block">
                    <strong class="text-success">&nbsp;</strong>
                    </div>
              </div>
            <?= form_close(); ?>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


